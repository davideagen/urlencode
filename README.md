# URL Encoding and Decoding

This project is an extremly simple URL encoder and decoder.

## Usage

Encode a string:

```bash
$ echo "Test string:" | urlencode
Test%20string%3A
```

Decode a string:

```bash
$ echo 'https%3A%2F%2Fwww.google.com' | urldecode
https://www.google.com
```

## Building

The project may be build with Bazel or Cargo.

### Building with Bazel

```bash
bazel build //...
```

Binaries can be found in the `bazel-bin/` directory.

### Building with Cargo

```bash
cargo build --release
```

The `urlencode` and `urldecode` binaries will be created in the `target/release/` directory.

## Testing

Like the builds, tests can be run through Bazel or Cargo.

### Testing with Bazel

```bash
bazel test//...
```

### Testing with Cargo

```bash
cargo test
```
