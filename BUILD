load("@crate_index//:defs.bzl", "all_crate_deps")
load("@rules_rust//rust:defs.bzl", "rust_binary", "rust_library", "rust_test")

package(default_visibility = ["//visibility:public"])

# Equivalent to the default Cargo release profileP:
# https://doc.rust-lang.org/cargo/reference/profiles.html
release_flags = [
    "-C",
    "codegen-units=16",
    "-C",
    "debuginfo=0",
    "-C",
    "debug-assertions=no",
    "-C",
    "incremental=false",
    "-C",
    "lto=fat",
    "-C",
    "overflow-checks=no",
    "-C",
    "panic=unwind",
    "-C",
    "rpath=no",
    "-C",
    "strip=debuginfo",
]

rust_library(
    name = "urlencode-lib",
    crate_name = "urlencode",
    srcs = [
        "src/lib.rs",
    ],
    deps = all_crate_deps(
        normal = True,
    ),
    proc_macro_deps = all_crate_deps(
        proc_macro = True,
    ),
    rustc_flags = release_flags,
)

rust_binary(
    name = "urlencode",
    srcs = ["src/bin/urlencode.rs"],
    deps = all_crate_deps(
        normal = True,
    ) + [
        ":urlencode-lib",
    ],
    proc_macro_deps = all_crate_deps(
        proc_macro = True,
    ),
    rustc_flags = release_flags,
)

rust_binary(
    name = "urldecode",
    srcs = ["src/bin/urldecode.rs"],
    deps = all_crate_deps(
        normal = True,
    ) + [
        ":urlencode-lib",
    ],
    proc_macro_deps = all_crate_deps(
        proc_macro = True,
    ),
    rustc_flags = release_flags,
)

rust_test(
  name = "unit_tests",
  crate = ":urlencode-lib",
  size = "small", # <1 min duration, max 20MB RAM, 1 CPU
)