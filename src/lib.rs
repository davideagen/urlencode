// https://stackoverflow.com/questions/37888042/remove-single-trailing-newline-from-string-without-cloning
// This removes one trailing \r\n or \n
// If there are multiple newlines, only the last one is stripped off.
// If there is no newline at the end of the string, the string is unchanged.
pub fn strip_trailing_newline(input: &str) -> &str {
    input
        .strip_suffix("\r\n")
        .or(input.strip_suffix('\n'))
        .unwrap_or(input)
}

#[test]
fn strip_newline_works() {
    assert_eq!(strip_trailing_newline("Test0\r\n\r\n"), "Test0\r\n");
    assert_eq!(strip_trailing_newline("Test1\r\nTest2"), "Test1\r\nTest2");
    assert_eq!(strip_trailing_newline("Test1\r\n"), "Test1");
    assert_eq!(strip_trailing_newline("Test2\n"), "Test2");
    assert_eq!(strip_trailing_newline("Test3"), "Test3");
}
