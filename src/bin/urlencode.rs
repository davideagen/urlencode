use std::io;
use urlencoding::encode;

use urlencode::strip_trailing_newline;

fn main() {
    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("Unable to read from stdin.");
    let encoded = encode(strip_trailing_newline(&buffer));
    println!("{}", encoded);
}
