use std::io;
use urlencoding::decode;

use urlencode::strip_trailing_newline;

fn main() {
    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("Unable to read from stdin.");
    let decoded = decode(strip_trailing_newline(&buffer)).expect("UTF-8");
    println!("{}", decoded);
}
